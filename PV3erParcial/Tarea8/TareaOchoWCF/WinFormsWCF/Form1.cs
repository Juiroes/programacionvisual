﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsWCF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var identificacion = textBox1.Text;

            using (MiServicio.MiServicioClient client = new MiServicio.MiServicioClient())
            {
                var persona = client.getPersona(identificacion);

                if (persona.Error != null)
                {
                    MessageBox.Show(persona.Error);
                }
                else
                {
                    string nombre = persona.Nombre;
                    int edad = persona.Edad;
                    string direccion = persona.Direccion;
                    string sexo = persona.Sexo;
                    string telefono = persona.Telefono;
                    MessageBox.Show("la identificacion de la persona es: " + identificacion + "" +
                        "\nSu nombre es:  " + nombre + "" +
                        "\nSu Edad es: " + edad + "" +
                        "\nSu direccion es : " + direccion + "" +
                        "\nSu sexo es: " + sexo + "" +
                        "\nSu telefono es: " + telefono + "");
                }
            }

        }
    }
}
