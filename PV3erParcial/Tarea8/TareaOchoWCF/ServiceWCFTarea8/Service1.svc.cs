﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiceWCFTarea8
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IMiServicio
    {
        public Persona getPersona(string identificacion)
        {
            if (identificacion == "0")
            {
                return new Persona() { Nombre = "Juan Isaac" , Edad = 19, Direccion = "Aquiles Serdan 496", Sexo = "Masculino", Telefono = "331434535"};
            }
            else if(identificacion == "1")
            {
                return new Persona() { Nombre = "Jose Ramon", Edad = 17 , Direccion = "La lomita jaja", Sexo = "Masculino", Telefono = "39349439"};
            }
            else
            {
                return new Persona() { Error = "Persona no encontrada" };

            }
        }
    }
}
