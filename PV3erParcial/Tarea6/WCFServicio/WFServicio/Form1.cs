﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFServicio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MiServicio.ServiceClient oClient = new MiServicio.ServiceClient();

            string a = oClient.GetData(5);

            MessageBox.Show(a);


            MiServicio.CompositeType oClass = new MiServicio.CompositeType();

            oClass.BoolValue = true;

            var resp = oClient.GetDataUsingDataContract(oClass);

            MessageBox.Show(resp.StringValue);
        }
    }
}
