﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace HelloService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "HelloService" en el código y en el archivo de configuración a la vez.

    public class HelloService : IHelloService
    {
        public string GetMessage(string name)
        {
            return "Hola" + name;
        }
    }
}
