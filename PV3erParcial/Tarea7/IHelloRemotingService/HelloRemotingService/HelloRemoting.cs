﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloRemotingService
{
    public class HelloRemoting : MarshalByRefObject, IHelloRemotingService.IHelloRemoting
    {
        public string getMessage(string name)
        {
            return "Hola " + name; ;
        }
    }
}
